// load the angular app
var Tweet = angular.module('Tweet', []);

// angular functionality for the main controller
function mainController($scope, $http) {
	// when landing on the page, get all orders and show them
	$http.get('/api/tweets')
		.success(function(data) {
			$scope.tweets = data;
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});
}