var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tweetSchema = new Schema({
	date : String,
	name : String,
	screen_name : String,
	text : String,
	profile_image_url : String
});

module.exports = mongoose.model('Tweet', tweetSchema);          
