# README #

This README documents the necessary steps to get this application up and running.

### What is this repository for? ###

* ALS Coding Challenge
* Using the Twitter API to get the last 50 tweets from @teacher2teacher, store them to mongoose, then display them to the webpage using the new mongoode DB.

### How do I get set up? ###

* Download folder
* Navigate to folder on Terminal.
* Download nodeJS at https://nodejs.org/en/download.
* Use the "npm install" command to install the node_modules needed for the application.
* Use the "node app" command to open your application.
* Navigate to http://localhost:8080/ your browser.